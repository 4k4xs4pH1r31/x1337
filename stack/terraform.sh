# AWS Terraform Template

This Stack is responsible to build the TEMPLATE stack in AWS.

## Architecture

- Diagram= Confluence/Wiki/Spaces/Dev/pages

## Resources

- VPC
- Subnets
- SG
- S3
- VPN

## Directory structure

- environments -> parameterization of resources.
  - source -> terraform code for resource creation.

## Configurations

- Environments DEV, UAT, PROD
  - Region: us-east-2
  - Terraform version > 0.15.4
  - Details in:
    - environments/dev.tfvars
    - environments/dev.tfbackend

    - environments/uat.tfvars
    - environments/uat.tfbackend

    - environments/prod.tfvars
    - environments/prod.tfbackend

## Versioning '.tfstate'

- Create S3 bucket: <Account-Number>-tfstate
  - example: xxxxxxx-tfstate
  
  - Details in:

    - environments/dev.tfbackend

    - environments/uat.tfbackend

    - environments/prod.tfbackend

## Variables in the directory

- This directory contains the parameters for each environment so we can leave the code flexible to apply in different environments.

  - <https://www.terraform.io/docs/backends/config.html>
  - <https://www.terraform.io/docs/configuration/variables.html>

### Environments

- dev.tfbackend -> backend parameterization.
- dev.tfvars -> features parameterization.

- uat.tfbackend -> backend parameterization.
- uat.tfvars -> features parameterization.

- prod.tfbackend -> backend parameterization.
- prod.tfvars -> features parameterization.

## Configured AWS Credentials

Configure your local stack first in your personal account and test before to move to the Project AWS account.

## Steps for run the project

  -THE SCRIPT OBJECTIVE !!!!!

### Example for Dev Environment

- Clone this Repo.

```bash
#!/bin/bash
cd ~/Documents && mkdir n1 && git clone https://gitlab.com/nirvana_tech/infrastructure/terraform/aws.git
```

- Shell permissions.

```bash
#!/bin/bash
sudo chmod +x ~/Documents/n1/aws/stack/terraform.sh
```

```bash
#!/bin/bash
sudo terraform workspace new n1_dev
```

- Workspace management, Isolate states, so if you run "terraform plan" Terraform will not see any existing state for this configuration.

```bash
#!/bin/bash
sudo terraform workspace new n1_dev
```

- Initialize, Prepare your working directory for other commands

```bash
#!/bin/bash
sudo ./terraform.sh dev init
```

- Plan, Show changes required by the current configuration

```bash
#!/bin/bash
sudo ./terraform.sh dev plan
```

- Apply, Create or update infrastructure

```bash
#!/bin/bash
sudo ./terraform.sh dev apply
```

- Destroy, previously-created infrastructure

```bash
#!/bin/bash
sudo ./terraform.sh dev destroy
```
