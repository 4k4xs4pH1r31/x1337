############################### Global Resources ###############################
variable "aws_region" {
  type = string
}


variable "env" {
  type = string
}

variable "key_name" {
  type = string
}

############################### p1tn Networking ##############################

variable "vpc_id_p1tn" {
  type = string
}

variable "subnets_p1tn" {
  type = list
}

############################### Navigator Networking ##############################

variable "vpc_id_navigator" {
  type = string
}

variable "subnets_navigator" {
  type = list
}

############################### p1tn ops ###############################

variable "bastion" {
  type = map
}