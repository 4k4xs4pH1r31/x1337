############################### Global Resources ###############################

aws_region = "us-east-2"
env        = "ops"

key_name   = "bastion"

############################### p1tn WebStore Networking ##############################

vpc_id_p1tn   = "vpc-10853074"

subnets_p1tn  = [
    "subnet-a1bf8e8a",
    "subnet-b2a14fc4",
    "subnet-41999818",
    "subnet-38bbff5d",
    "subnet-bf43e682",
    "subnet-7315887f"
]

############################### p1tn Eureka Networking ##############################

vpc_id_navigator  = "vpc_465c573d"

subnets_navigator  = [
    "subnet-0143dbe23c6249ae0",
    "subnet-01f1e364c673e11b3",
    "subnet-085727bf230c9eda4",
    "subnet-0965ce14397171382",
    "subnet-0c5186c8637351863",
    "subnet-0f2fee05cee34b698",
    "subnet-5cc30800",
    "subnet-6d188727",
    "subnet-979c4af0",
    "subnet-d5ae49eb",
    "subnet-db1cc9f5",
    "subnet-f25ccefd"
]

############################### p1tn ops ###############################

bastion = {
    name           = "bastion"
    ami            = "ami-01884af3847318c5c"
    instance_count = 1 
    instance_type  = "t3.micro"
}