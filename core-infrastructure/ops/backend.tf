terraform {
  backend "s3" {
    bucket = "p1tnops-infrastructure-state"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }
}

