#!/bin/bash

ciphertext=$(aws kms decrypt --ciphertext-blob fileb://<(echo "$1" | base64 -d) --output text --query Plaintext)

echo "${ciphertext}" | base64 --decode

