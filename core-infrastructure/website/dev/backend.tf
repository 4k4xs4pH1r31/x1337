terraform {
  backend "s3" {
    bucket = "dev-website-p1tn-core-infrastructure-state"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }
}