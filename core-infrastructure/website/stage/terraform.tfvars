############################### Global Resources ###############################

aws_region = "us-east-2"
env        = "stage"

key_name   = "p1tn-p1tn-ops"

############################### p1tn Networking ##############################

vpc_id   = "vpc-10853074"
subnets  = [
    "subnet-a1bf8e8a",
    "subnet-b2a14fc4",
    "subnet-41999818",
    "subnet-38bbff5d",
    "subnet-bf43e682",
    "subnet-7315887f"
]

############################### p1tn Databases ###############################

database = {
    name           = "p1tn-database"
    ami            = "ami-0d4701aff3eec9b21"
    instance_count = 1 
    instance_type  = "t3.micro"
}

############################### p1tn Website ###############################

website = {
    name = "p1tn-website"
}

website_node_1 = {
    name           = "p1tn-website-node-1"
    ami            = "ami-0af34a2110b22d7f7"
    instance_count = 1
    instance_type  = "t3.micro"
}

website_node_2 = {
    name           = "p1tn-website-node-2"
    ami            = "ami-0a7a2d3d2271de942"
    instance_count = 1
    instance_type  = "t3.micro"
}

############################### p1tn Load Balancer HAProxy ###############################

haproxy = {
    name           = "p1tn-load-balancer"
    ami            = "ami-09a6b3ea8da0df745"
    instance_count = 1
    instance_type  = "t3.micro"
}

############################### p1tn ElasticSearch ###############################

elasticsearch = {
    name           = "p1tn-elasticsearch"
    ami            = "ami-0a180cc400f5605a1"
    instance_count = 1
    instance_type  = "t3.micro"
}