############################### Global Resources ###############################
variable "aws_region" {
}

variable "env" {
  type = string
}

variable "key_name" {
  type = string
}

############################### p1tn Networking ##############################

variable "vpc_id" {
  type = string
}

variable "subnets" {
  type = list
}

############################### p1tn Databases ###############################

variable "database" {
  type = map
}

############################### p1tn Website ###############################

variable "website" {
  type = map
}

variable "website_node_1" {
  type = map
}

variable "website_node_2" {
  type = map
}

############################### p1tn Load Balancer HAProxy ###############################

variable "haproxy" {
  type = map
}

############################### p1tn ElasticSearch ###############################

variable "elasticsearch" {
  type = map
}