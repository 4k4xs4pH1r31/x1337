############################### Global Resources ###############################

aws_region = "us-east-2"
env        = "prod"

key_name   = "p1tn-p1tn-ops"

############################### p1tn Networking ##############################

vpc_id   = "vpc-10853074"
subnets  = [
    "subnet-a1bf8e8a",
    "subnet-b2a14fc4",
    "subnet-41999818",
    "subnet-38bbff5d",
    "subnet-bf43e682",
    "subnet-7315887f"
]

############################### p1tn Databases ###############################

database = {
    name           = "p1tn-database"
    ami            = "ami-061b86044c55bd56d"
    instance_count = 1 
    instance_type  = "t3.micro"
}