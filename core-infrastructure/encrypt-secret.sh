#!/bin/sh

##################################################################
# This script encrypts a given string with the Ops KMS key       #
#                                                                #
# Use this to generate encrypted text inside terraform varialbes #
# using aws_kms_secrets data resource                            #
##################################################################

OPS_KMS_ID=$(aws kms list-aliases --output text --query 'Aliases[?AliasName==`alias/RDSp1tnEurekaNavigator`].TargetKeyId')

aws kms encrypt --key-id ${OPS_KMS_ID} --plaintext "$1" --output text --query CiphertextBlob


aws kms encrypt --key-id eb633fcb-756e-44ac-b0fe-6915c4a50b76 --plaintext fileb://x.txt --output text --query CiphertextBlob